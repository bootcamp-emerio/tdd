## Test Driven Development (TDD)

Apa itu *Test Driven Development (TDD)* dan apa sih keuntungan kalau kita menjalankan *Test Driven Development* ? Sebelum kita membahas jauh apa itu TDD, saya ingin menjelaskan latar belakang kenapa TDD dibutuhkan dalam membuat pengembangan *software*.

### **Pengembangan software secara konvensional**
Umumnya pada sebuah pembuatan *software* akan langsung menulis kode sesuai dengan *software requirements* yang tersedia, setelah selesai membuat kode maka kita tes aplikasi kita apakah berjalan baik dan apabila berjalan dengan sangat baik maka akan diberikan ke *client* untuk digunakan pada *production environment* dan development selesai.

### **oke itu hanya sebuah ekspektasi, lalu bagaimanakah realitanya ?**

![](./image/meme.jpg)

realitanya adalah sering kali *software requirement* berubah dari kesepakatan awal, hal ini sangatlah sering atau bisa dikatakan selalu terjadi. lalu masalah apa yang terjadi dengan perubahan *software requirement* tersebut ?

 - Penambahan fitur dapat membuat *break* kode yang sudah ada atau membuat bugs baru.
 - kode yang *break* dan *bugs* seringkalinya tidak pernah terdeteksi sehingga terbawa saat instalasi software di level produksi.
- penemuan bugs secara manual memerlukan waktu yang lama dan sangat melelahkan.

### **Lalu solusinya ?**

solusinya adalah pembuatan *automated test script* untuk mengecek apakah setiap baris kode yang dibuat tidak merusak kode yang ada ataupun menambahkan *bugs* yang baru agar berjalan sesuai dengan rencana yang telah direncanakan. hal ini akan mempercepat untuk melakukan pengetesan karena dilakukan oleh komputer dengan menggunakan *test script* yang telah dibuat. namun ada timbal balik dari hal ini, yaitu investasi waktu yang lebih lama karena selain membuat kode program, *developer* juga harus mebuat kode *script test*.

### **Working Code dulu, Automated Test Kemudian ?**

oke kita punya solusi untuk *automated testing*. kita akan mencoba untuk membuat alur dimana pertama kali membuat kode program lalu membuat *automated testing*. berdasarkan beberapa pengalaman, ada beberapa hal yang menjadi permasalahan pada metode ini.

- over engineering : setelah membuat kode program kita baru menyadari bahwa apa yang telah kita buat lebih kompleks dari kebutuhan yang diperlukan.
- kode yang telah ditulis sulit untuk dibuat tesnya, beberapa hal buruk bisa terjadi seperti devloper mengakali test agar sesuai dengan kode yang telah dibuat.
- kita jadi malas membuat test karena kompleksitas aplikasi yang terlalu tinggi.

Maka dari itu, saat ini kita coba untuk mengubah alur, test dulu lalu *working code* kemudian. Hal ini yang disebut dengan *Test Driven Development*

### **TDD (Test Driven Development)**

Sesuai dengan namanya *Test Driven Development* adalah pengembangan software yang disetir oleh test, singkat kata kita wajib membuat test terlebih dahulu lalu membuat kode program. implementasinya sebagai berikut:

- sebelum memuat kode program buatlah test terlebih dahulu. pastikan semua kemungkinan dituangkan dalam test (positif dan negatif test).
- jalankan test dan pastikan test gagal semua saat pertama kali.
- pembuatan kode program sesuai dengan script test yang dibuat.
- bila merasa bahwa kode berantakan, *refactor the code* lalu test lagi , ketika test aman maka tidak ada masalah
- ulangi dari awal


### **Implementasi TDD pada spring boot**

1. Overview Project 

    Pada kali ini kita akan membuat sebuah project *CRUD (Create Read Update Delete)* dengan menggunakan spring boot dan satu buah *entity* sebagai object percobaan *CRUD*. Entity yang akan dibuat adalah sebuah customer dimana terdapat atribut nama, alamat, email , dan jenis kelamin. Penjelasan detail terkait apa itu spring boot dan bagaimana cara menggunakannya dapat dilihat lebih detail pada *link* berikut [spring boot](https://gitlab.com/bootcamp-emerio/tdd/blob/master/spring-boot/dokumentasi.md)

2. Skema test 
	
	Berdasarkan penjelasan diatas bahwa tahap pertama yang dilakukan pada TDD adalah membuat test terlebih dahulu, maka dari itu hal yang pertama kita lakukan adalah membuat skema test dari fitur yang harus dibuat.
	
| Fungsi  |  Positif Test | Negatif Test  | 
|:-:|:-:|:-:|
| Create  |  Membuat sebuah customer dengan seluruh atribut |  Membuat sebuah customer dengan sebagian atribut | 
|   |   | Membuat sebuah customer namun tanpa tanda '@' pada atribut email  | 
|   |   | Membuat sebuah customer dengan jenis kelamin bukan pria ataupun wanita  | 
|   |   | Membuat sebuah customer dengan nama atau email yang telah di registrasi  | 
| Read  | Mengembalikan seluruh list dari customer  | Mencoba mengembalikan data customer yang tidak terdaftar  | 
|  | Mengembalikan sebuah data dari customer sesuai dengan id terkait |  | 
|Update|Update sebuah customer dengan mengubah semua atribut|Update sebuah customer dengan nama dan email dari customer lain yang telah teregistrasi|
||Update sebuah customer dengan mengubah sebagian atribut|Update sebuah customer pada atribut email tapi tanpa tanda '@'|
|||Update sebuah customer yang tidak terdaftar|
|Delete|Menghapus user yang telah teregistrasi|Menghapus user yang tidak tersedia|
				


3. Membuat *Entity Customer* dan *Crud Repository*
	
	Dikarenakan pada test kita membutuhkan input berupa data customer maka sebelum memulai membuat test hal yang dilakukan adalah membuat entity customer dan crud repository entity tersebut.
	
	```java
	@Entity
	public class Customer {
	
	    @Id
	    @GeneratedValue
	    private Long id;
	
	    @Column(nullable=false)
	    private String name;
	
	    @Column(nullable=false)
	    private String alamat;
	
	
	    @Column(nullable=false)
	    @Email
	    private String email;
	
	    @Column(nullable=false)
	    private String jenisKelamin;
	
		 // setter and getter
	    public Long getId(){
	        return this.id;
	    }
	
	    public void setId(Long id){
	        this.id = id;
	    }
	    
	    public String getName(){
	        return this.name;
	    }
	
	    public void setName(String name){
	        this.name = name;
	    }
	
	    public String getAlamat(){
	        return this.alamat;
	    }
	
	    public void setAlamat(String alamat){
	        this.alamat = alamat;
	    }
	
	    public String getEmail(){
	        return this.email;
	    }
	
	    public void setEmail(String email){
	        this.email = email;
	    }
	
	    public String getJenisKelamin(){
	        return this.jenisKelamin;
	    }
	
	    public void setJenisKelamin(String jenisKelamin){
	        this.jenisKelamin = jenisKelamin;
	    }
	
	}
	```
	
	```java
	package com.emerio.rnd.pair.customerservice.repo;

	import java.util.Optional;
	import com.emerio.rnd.pair.customerservice.entity.Customer;
	import org.springframework.data.repository.CrudRepository;
	
	public interface CustomerRepository extends CrudRepository<Customer, Long>{
		Optional<Customer> findByName(String name);
	}
	```

4. Membuat *Unit Test*
	Sesuai dengan apa yang kita ingin implementasikan pada TDD ini, maka kita akan membuat test berdasarkan skema yang telah dibuat pada poin nomor 2. Namun disini saya tidak akan membuat semua test, saya hanya membuat bagian positif test dan sisanya diharapkan bisa kalian lanjutkan. 
	
	
	```java
	package com.emerio.rnd.pair.customerservice;

	import static org.junit.Assert.assertNotNull;
	
	import java.io.IOException;
	import java.nio.charset.Charset;
	import java.util.Arrays;
	
	import com.emerio.rnd.pair.customerservice.entity.Customer;
	import com.emerio.rnd.pair.customerservice.repo.CustomerRepository;
	
	import org.junit.Before;
	import org.junit.Test;
	import org.junit.runner.RunWith;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.boot.test.context.SpringBootTest;
	import org.springframework.http.MediaType;
	import org.springframework.http.converter.HttpMessageConverter;
	import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
	import org.springframework.mock.http.MockHttpOutputMessage;
	import org.springframework.test.context.junit4.SpringRunner;
	import org.springframework.test.context.web.WebAppConfiguration;
	import org.springframework.test.web.servlet.MockMvc;
	import org.springframework.test.web.servlet.ResultActions;
	import org.springframework.web.context.WebApplicationContext;
	import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
	
	import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
	import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
	import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
	import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
	import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
	import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
	import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
	
	@RunWith(SpringRunner.class)
	@SpringBootTest(classes = CustomerServiceApplication.class)
	@WebAppConfiguration
	public class CustomerServiceApplicationTests {
		private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
		MediaType.APPLICATION_JSON.getSubtype(),
		Charset.forName("utf8"));
	
		private MockMvc mockMvc;
	
		private String URIcustomer = "/customers";
	
		private HttpMessageConverter mappingJackson2HttpMessageConverter;
	
		@Autowired
		private CustomerRepository customerRepository;
	
		@Autowired
		private WebApplicationContext webApplicationContext;
	
		@Autowired
		void setConverters(HttpMessageConverter<?>[] converters) {
	
			this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
				.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
				.findAny()
				.orElse(null);
	
			assertNotNull("the JSON message converter must not be null",
					this.mappingJackson2HttpMessageConverter);
		}
	
		
		private Customer customer1;
		private Customer customer2;
		private Customer customer3;
		private Customer customer4;
		private Customer customer5;
		private Customer customer6;
		private Customer customer7;
	
		@Before()
		public void setup() throws Exception {
			this.mockMvc = webAppContextSetup(webApplicationContext).build();
	
	
			this.customer1 = new Customer();
			this.customer1.setName("djeroWcik");
			this.customer1.setAlamat("pamulanx");
			this.customer1.setEmail("djeroWcik@pamulanxcorp.com");
			this.customer1.setJenisKelamin("pria");
	
			this.customer2 = new Customer();
			this.customer2.setName("djero");
		
			this.customer5 = new Customer();
			this.customer5.setName("djero");
			this.customer5.setAlamat("pamulanx");
			this.customer5.setEmail("djero@pamulanxcorp.com");
			this.customer5.setJenisKelamin("pria");
	
			this.customer6 = new Customer();
			this.customer6.setName("djero");
			this.customer6.setAlamat("pamulanxzz");
			this.customer6.setEmail("djeroxx@pamulanxcorp.com");
			this.customer6.setJenisKelamin("pria");
	
			this.customer7 = new Customer();
			this.customer7.setName("djeroW");
			this.customer7.setAlamat("pamulanxxxxxx");
			this.customer7.setEmail("djeroW@pamulanxcorp.com");
			this.customer7.setJenisKelamin("pria");
	
			this.customer7=customerRepository.save(customer7);
			this.customer5=customerRepository.save(customer5);
		}
	
	
		//create-test
		@Test
		public void createUser() throws Exception {
			mockMvc.perform(post(this.URIcustomer)
					.content(this.json(customer1))
					.contentType(contentType))
					.andExpect(status().isCreated());
		}
		
		//read-test
		@Test
		public void readUserAll() throws Exception {
			ResultActions ra = mockMvc.perform(get(this.URIcustomer));
			ra.andExpect(content().contentType(contentType));
			ra.andExpect(status().isOk());
		}
	
		@Test
		public void readUserA() throws Exception {
			ResultActions ra = mockMvc.perform(get(this.URIcustomer+"/"+this.customer5.getId()));
			ra.andExpect(content().contentType(contentType));
			ra.andExpect(status().isOk());
			ra.andExpect(content().json(this.json(customer5)));
		}
		
		//update-test
		@Test
		public void updateUserFull() throws Exception {
			mockMvc.perform(put(this.URIcustomer+"/"+this.customer5.getId())
			.content(this.json(customer6))
			.contentType(contentType))
			.andExpect(status().isCreated());
		}
	
		@Test
		public void updateUserNotFull() throws Exception {
			mockMvc.perform(put(this.URIcustomer+"/"+this.customer5.getId())
			.content(this.json(customer2))
			.contentType(contentType))
			.andExpect(status().isCreated());
		}
		
		//delete-test
		@Test
		public void delete() throws Exception {
			ResultActions rada = mockMvc.perform(delete(this.URIcustomer+"/"+this.customer7.getId()));
				rada.andExpect(status().isOk());
		}
	
	    protected String json(Object o) throws IOException {
	        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
	        this.mappingJackson2HttpMessageConverter.write(
	                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
	        return mockHttpOutputMessage.getBodyAsString();
	    }
	
	}
	```
	Kode diatas adalah unit test yang dibuat untuk kebutuhan CRUD (hanya positif test), maka apabila dijalankan `mvn test` maka hasilnya adalah error/failure sebanyak 6 (sesuai dengan jumlah test yang dibuat. Diharapkan kalian melanjutkan test yang negatif sesuai dengan yang telah dideskripsikan pada tabel skema test. Baru selanjutnya membuat *working code* berdasarkan test yang telah didefinisikan.
	
5. Membuat *Working Code*

	```java
	package com.emerio.rnd.pair.customerservice.controller;
	
	import java.util.List;
	import java.util.Optional;
	
	import com.emerio.rnd.pair.customerservice.entity.Customer;
	import com.emerio.rnd.pair.customerservice.repo.CustomerRepository;
	
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.web.bind.annotation.CrossOrigin;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.RequestBody;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RestController;
	import org.springframework.web.bind.annotation.RequestMethod;
	import org.springframework.http.HttpStatus;
	import org.springframework.http.MediaType;
	import org.springframework.http.ResponseEntity;
	
	@RestController
	public class CustomerController {
	
	    private CustomerRepository customerRepository;
	    @Autowired
	    CustomerController (CustomerRepository customerRepository)
	    {
	        this.customerRepository = customerRepository;
	    }
	
	    @RequestMapping(method = RequestMethod.GET, value="/customers", produces = MediaType.APPLICATION_JSON_VALUE)
	    ResponseEntity<Iterable<Customer>> getAll ()
	    {
	        try
	        {
	            return new ResponseEntity<Iterable<Customer>>(this.customerRepository.findAll(), HttpStatus.OK);
	        } catch (Exception e)
	        {
	            e.printStackTrace();
	            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	        }
	    }
	
	    @RequestMapping(method = RequestMethod.GET, value="/customers/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	    ResponseEntity<Customer> getOne (@PathVariable Long id)
	    {
	        try
	        {
	            return new ResponseEntity<Customer>(this.customerRepository.findById(id).get() , HttpStatus.OK);
	        } catch (Exception e)
	        {
	            e.printStackTrace();
	            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	        }
	    }
	
	    @RequestMapping(method = RequestMethod.POST, value="/customers", produces = MediaType.APPLICATION_JSON_VALUE)
	    ResponseEntity<Customer> createOne(@RequestBody Customer cust)
	    {
	        try
	        {
	            Optional<Customer> customer = this.customerRepository.findByName(cust.getName());
	            if (customer.isPresent())
	            {
	                return new ResponseEntity<Customer>(new Customer(), HttpStatus.BAD_REQUEST);
	            } else 
	            {
	                Customer newCust =  this.customerRepository.save(cust);
	                System.out.println("data:" +newCust);
	                return new ResponseEntity<Customer>(newCust, HttpStatus.CREATED);
	            }
	        } catch (Exception e )
	        {
	            e.printStackTrace();
	            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	        }
	    }
	
	    @RequestMapping(method = RequestMethod.PUT, value="/customers/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	    ResponseEntity<Customer> updateOne (@PathVariable Long id, @RequestBody Customer cust)
	    {
	        try 
	        {
	            Optional<Customer> customer = this.customerRepository.findById(id);
	            if (customer.isPresent())
	            {
	                Customer custUpdate = customer.get();
	                if (cust.getName() != null)
	                {
	                    custUpdate.setName(cust.getName());
	                }
	                if (cust.getAlamat() != null)
	                {
	                    custUpdate.setAlamat(cust.getAlamat());
	                }
	                if (cust.getEmail() != null)
	                {
	                    custUpdate.setEmail(cust.getEmail());
	                }
	                if (cust.getJenisKelamin() != null)
	                {
	                    custUpdate.setJenisKelamin(cust.getJenisKelamin());
	                }
	                custUpdate = this.customerRepository.save(custUpdate);
	                return new ResponseEntity<Customer>(custUpdate, HttpStatus.CREATED);
	            } else 
	            {
	                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	            }
	        } catch (Exception e)
	        {
	            e.printStackTrace();
	            return null;
	        }
	    }
	
	    @RequestMapping(method = RequestMethod.DELETE, value = "/customers/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	    ResponseEntity<Customer> deleteOne (@PathVariable Long id)
	    {
	        try
	        {
	            Optional<Customer> custDelete = this.customerRepository.findById(id);
	            if (custDelete.isPresent())
	            {
	                this.customerRepository.delete(custDelete.get());
	                return new ResponseEntity<Customer>(custDelete.get(), HttpStatus.OK);
	            } else
	            {
	                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	            }
	        } catch (Exception e )
	        {
	            e.printStackTrace();
	            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	        }
	    }
	}
	```
	
	Diatas adalah *working code* pada *controller* dimana fungsi CRUD bisa dijalankan. untuk mengetahui apakah kode ini sesuai dengan test dapat kita jalankan script `mvn test` dan dimana hasil yang diharapkan adalah 0 untuk failure/error.	
	
6. Test Coverage

	Setelah memastikan bahwa test yang dibuat telah lulus semua, maka selanjutnya adalah melihat apakah test yang kita buat telah meng-*cover* seluruh kode yang dibuat. Untuk mendapatkan hal tersebut kita menggunakan sonarqube sebagai platform untuk menganalisa coverage test. 
	Untuk instalasi sonarqube dapat dilihat pada link berikut [sonarque](https://docs.sonarqube.org/display/SONAR/Installing+the+Server). Dan untuk mendapatkan coverage test maka kita dapat menjalankan script maven berikut .
	
	```mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent install -Dmaven.test.failure.ignore=false package sonar:sonar -Dsonar.host.url=http://{host-sonar}:{port-sonar}```
	
	Berikut adalah contoh result coverage test yang telah dilakukan dimana hasilnya sebesar 82.1%, 
	![](./image/sonar.png)
	
	
	dan kita dapat melihat mana kode yang telah dicover test mana yang belum seperti gambar berikut.
	
	![](./image/sonarqube3.png)
	![](./image/sonarqube4.png)
	
