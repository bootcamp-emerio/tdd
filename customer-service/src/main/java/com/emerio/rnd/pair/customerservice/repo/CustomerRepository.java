package com.emerio.rnd.pair.customerservice.repo;

import java.util.Optional;

import com.emerio.rnd.pair.customerservice.entity.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Long>{
	Optional<Customer> findByName(String name);

}