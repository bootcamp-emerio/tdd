package com.emerio.rnd.pair.customerservice.controller;

import java.util.List;
import java.util.Optional;

import com.emerio.rnd.pair.customerservice.entity.Customer;
import com.emerio.rnd.pair.customerservice.repo.CustomerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

@RestController
@CrossOrigin
public class CustomerController {

    private CustomerRepository customerRepository;
    @Autowired
    CustomerController (CustomerRepository customerRepository)
    {
        this.customerRepository = customerRepository;
    }

    @RequestMapping(method = RequestMethod.GET, value="/customers", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Iterable<Customer>> getAll ()
    {
        try
        {
            return new ResponseEntity<Iterable<Customer>>(this.customerRepository.findAll(), HttpStatus.OK);
        } catch (Exception e)
        {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.GET, value="/customers/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Customer> getOne (@PathVariable Long id)
    {
        try
        {
            return new ResponseEntity<Customer>(this.customerRepository.findById(id).get() , HttpStatus.OK);
        } catch (Exception e)
        {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value="/customers", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Customer> createOne(@RequestBody Customer cust)
    {
        try
        {
            Optional<Customer> customer = this.customerRepository.findByName(cust.getName());
            if (customer.isPresent())
            {
                return new ResponseEntity<Customer>(new Customer(), HttpStatus.BAD_REQUEST);
            } else 
            {
                Customer newCust =  this.customerRepository.save(cust);
                System.out.println("data:" +newCust);
                return new ResponseEntity<Customer>(newCust, HttpStatus.CREATED);
            }
        } catch (Exception e )
        {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = RequestMethod.PUT, value="/customers/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Customer> updateOne (@PathVariable Long id, @RequestBody Customer cust)
    {
        try 
        {
            Optional<Customer> customer = this.customerRepository.findById(id);
            if (customer.isPresent())
            {
                Customer custUpdate = customer.get();
                if (cust.getName() != null)
                {
                    custUpdate.setName(cust.getName());
                }
                if (cust.getAlamat() != null)
                {
                    custUpdate.setAlamat(cust.getAlamat());
                }
                if (cust.getEmail() != null)
                {
                    custUpdate.setEmail(cust.getEmail());
                }
                if (cust.getJenisKelamin() != null)
                {
                    custUpdate.setJenisKelamin(cust.getJenisKelamin());
                }
                custUpdate = this.customerRepository.save(custUpdate);
                return new ResponseEntity<Customer>(custUpdate, HttpStatus.CREATED);
            } else 
            {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/customers/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Customer> deleteOne (@PathVariable Long id)
    {
        try
        {
            Optional<Customer> custDelete = this.customerRepository.findById(id);
            if (custDelete.isPresent())
            {
                this.customerRepository.delete(custDelete.get());
                return new ResponseEntity<Customer>(custDelete.get(), HttpStatus.OK);
            } else
            {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e )
        {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
