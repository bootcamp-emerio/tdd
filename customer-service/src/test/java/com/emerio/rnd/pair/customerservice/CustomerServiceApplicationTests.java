package com.emerio.rnd.pair.customerservice;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import com.emerio.rnd.pair.customerservice.entity.Customer;
import com.emerio.rnd.pair.customerservice.repo.CustomerRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.context.WebApplicationContext;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CustomerServiceApplication.class)
@WebAppConfiguration
public class CustomerServiceApplicationTests {
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
	MediaType.APPLICATION_JSON.getSubtype(),
	Charset.forName("utf8"));

	private MockMvc mockMvc;

	private String URIcustomer = "/customers";

	private HttpMessageConverter mappingJackson2HttpMessageConverter;

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	void setConverters(HttpMessageConverter<?>[] converters) {

		this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
			.filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
			.findAny()
			.orElse(null);

		assertNotNull("the JSON message converter must not be null",
				this.mappingJackson2HttpMessageConverter);
	}

	
	private Customer customer1;
	private Customer customer2;
	private Customer customer3;
	private Customer customer4;
	private Customer customer5;
	private Customer customer6;
	private Customer customer7;

	@Before()
	public void setup() throws Exception {
		this.mockMvc = webAppContextSetup(webApplicationContext).build();


		this.customer1 = new Customer();
		this.customer1.setName("djeroWcik");
		this.customer1.setAlamat("pamulanx");
		this.customer1.setEmail("djeroWcik@pamulanxcorp.com");
		this.customer1.setJenisKelamin("pria");

		this.customer2 = new Customer();
		this.customer2.setName("djero");

		this.customer3 = new Customer();
		this.customer3.setName("djero");
		this.customer3.setAlamat("pamulanx");
		this.customer3.setEmail("djeropamulanxcorp.com");
		this.customer3.setJenisKelamin("pria");

		this.customer4 = new Customer();
		this.customer4.setName("djero");
		this.customer4.setAlamat("pamulanx");
		this.customer4.setEmail("djero@pamulanxcorp.com");
		this.customer4.setJenisKelamin("wapri");

		this.customer5 = new Customer();
		this.customer5.setName("djero");
		this.customer5.setAlamat("pamulanx");
		this.customer5.setEmail("djero@pamulanxcorp.com");
		this.customer5.setJenisKelamin("pria");

		this.customer6 = new Customer();
		this.customer6.setName("djero");
		this.customer6.setAlamat("pamulanxzz");
		this.customer6.setEmail("djeroxx@pamulanxcorp.com");
		this.customer6.setJenisKelamin("pria");

		this.customer7 = new Customer();
		this.customer7.setName("djeroW");
		this.customer7.setAlamat("pamulanxxxxxx");
		this.customer7.setEmail("djeroW@pamulanxcorp.com");
		this.customer7.setJenisKelamin("pria");

		this.customer7=customerRepository.save(customer7);
		this.customer5=customerRepository.save(customer5);
	}


	//create-test
	@Test
	public void createUser() throws Exception {
		mockMvc.perform(post(this.URIcustomer)
				.content(this.json(customer1))
				.contentType(contentType))
				.andExpect(status().isCreated());
	}

	@Test
	public void createUserB() throws Exception {
		mockMvc.perform(post(this.URIcustomer)
				.content(this.json(customer2))
				.contentType(contentType))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void createUserC() throws Exception {
		mockMvc.perform(post(this.URIcustomer)
				.content(this.json(customer3))
				.contentType(contentType))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void createUserD() throws Exception {
		mockMvc.perform(post(this.URIcustomer)
				.content(this.json(customer4))
				.contentType(contentType))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void createUserE() throws Exception {
		mockMvc.perform(post(this.URIcustomer)
				.content(this.json(customer5))
				.contentType(contentType))
				.andExpect(status().isBadRequest());
	}


	//read-test
	@Test
	public void readUserAll() throws Exception {
		ResultActions ra = mockMvc.perform(get(this.URIcustomer));
		ra.andExpect(content().contentType(contentType));
		ra.andExpect(status().isOk());
	}

	@Test
	public void readUserA() throws Exception {
		ResultActions ra = mockMvc.perform(get(this.URIcustomer+"/"+this.customer5.getId()));
		ra.andExpect(content().contentType(contentType));
		ra.andExpect(status().isOk());
		ra.andExpect(content().json(this.json(customer5)));
	}

	@Test
	public void readUserU() throws Exception {
		ResultActions ra = mockMvc.perform(get(this.URIcustomer+"/100"));
		ra.andExpect(status().isNotFound());
	}


	@Test
	public void readUserNull() throws Exception {
		ResultActions ra = mockMvc.perform(get(this.URIcustomer+"/null"));
		ra.andExpect(status().isBadRequest());
	}


	//update-test
	@Test
	public void updateUser5() throws Exception {
		mockMvc.perform(put(this.URIcustomer+"/"+this.customer5.getId())
		.content(this.json(customer6))
		.contentType(contentType))
		.andExpect(status().isCreated());
	}

	@Test
	public void updateUser5Not() throws Exception {
		mockMvc.perform(put(this.URIcustomer+"/"+this.customer5.getId())
		.content(this.json(customer2))
		.contentType(contentType))
		.andExpect(status().isCreated());
	}

	@Test
	public void updateUser57() throws Exception {
		mockMvc.perform(put(this.URIcustomer+"/"+this.customer7.getId())
		.content(this.json(customer5))
		.contentType(contentType))
		.andExpect(status().isCreated());
	}

	// @Test
	// public void updateUser53() throws Exception {
	// 	mockMvc.perform(put(this.URIcustomer+"/"+this.customer5.getId())
	// 	.content(this.json(customer3))
	// 	.contentType(contentType))
	// 	.andExpect(status().isBadRequest());
	// }

	@Test
	public void updateUser15() throws Exception {
		mockMvc.perform(put(this.URIcustomer+"/100")
		.content(this.json(customer5))
		.contentType(contentType))
		.andExpect(status().isNotFound());
	}

	@Test
	public void updateUserNull() throws Exception {
		mockMvc.perform(put(this.URIcustomer+"/Null")
		.content(this.json(customer5))
		.contentType(contentType))
		.andExpect(status().isBadRequest());
	}

	//delete-test
	@Test
	public void delete7() throws Exception {
		ResultActions rada = mockMvc.perform(delete(this.URIcustomer+"/"+this.customer7.getId()));
			rada.andExpect(status().isOk());
	}

	@Test
	public void delete1() throws Exception {
		ResultActions rada = mockMvc.perform(delete(this.URIcustomer+"/100"));
			rada.andExpect(status().isNotFound());
	}

	@Test
	public void deleteNull() throws Exception {
		ResultActions rada = mockMvc.perform(delete(this.URIcustomer+"/null"));
			rada.andExpect(status().isBadRequest());
	}

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}
