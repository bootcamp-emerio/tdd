## Spring Boot

#### Prerequisite
Untuk Mengikuti tutorial ini maka ada dua hal yang harus diinstall yaitu java 8 dan maven, untuk mengecek apakah java telah terinstall dapat menggunakan script berikut pada terminal

```
java -version
```
dan akan keluar hasil sebagai berikut

```
java version "1.8.0_161"
Java(TM) SE Runtime Environment (build 1.8.0_161-b12)
Java HotSpot(TM) 64-Bit Server VM (build 25.161-b12, mixed mode)
```

dan untuk maven 

```
mvn -version
```

dan akan keluar hasil sebagai berikut
```
Apache Maven 3.5.3 (3383c37e1f9e9b3bc3df5050c29c8aff9f295297; 2018-02-25T02:49:05+07:00)
Maven home: /usr/local/Cellar/maven/3.5.3/libexec
Java version: 1.8.0_161, vendor: Oracle Corporation
Java home: /Library/Java/JavaVirtualMachines/jdk1.8.0_161.jdk/Contents/Home/jre
Default locale: en_ID, platform encoding: UTF-8
OS name: "mac os x", version: "10.14", arch: "x86_64", family: "mac"
```

#### Overview
Spring boot adalah framework yang memudahkan untuk membuat project spring seakan hanya tinggal *run* aplikasi. spring mendukung untuk menggunakan *third party library* dengan menggunakan maven ataupun gradle.

#### Membuat *Project*

Spring boot menyediakan [portal](https://start.spring.io) untuk mulai project baru. Pada portal tersebut *project* dapat dibuat dengan menggunakan *maven* atau dengan *gradle*, dan terdapat 3 bahasa pemrograman yang dapat digunakan yaitu java, kotlin, dan groovy. Pada tulisan ini kita akan mencoba membuat *project* dengan menggunakan *maven* dan *java*.

![](./images/start.spring.io.png)

Gambar diatas adalah tampilan dari portal dimana kita dapat menambahkan *depedencies* sesuai dengan apa yang kita butuhkan dan untuk mendownload project dapat menekan tombol *Generate Project*.

#### Struktur Project 

Setelah berhasil generate project dan mengekstrak file maka kita dapat melihat struktur dari project tersebut.

```
|-- .mvn 
|-- .settings
|-- src
	|-- main 
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- rest
						|-- RestApplication.java
		|-- resources
			|-- static
			|-- templates
			|-- application.properties
	|-- test
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- rest
						|-- RestApplicationTest.java
|-- target
|-- .classpath
|-- .gitignore
|-- .project
|-- mvnw
|-- mvnw.cmd
|-- pom.xml

```

Pada struktur gambar diatas ada dua bagian yang akan sering kita ubah yaitu pada bagian **src** dan **pom.xml**. Berikut adalah penjelasan dari kedua bagian tersebut.

- **src**
	
	Pada bagian ini terdapat 2 bagian utama yaitu *main* dan *test*. *main* pada umumnya adalah tempat dimana kode java akan diletakan disana, dalam main sendiri terbagi dua yaitu java untuk kode java yang akan dibuat dan resources sebagai tempat konfigurasi project,
	
- **pom.xml**
	
	Bagian ini akan berisi *depedency* library yang akan digunakan. seperti contoh bagian kode dibawah ini maka kita menggunakan library spring-boot-starter-data-jpa, spring-boot-starter-web, h2, dan spring-boot-starter-test.
	
	```xml
	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<scope>runtime</scope>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>
	```
	
#### *Practice (Rest API Service)*

Setelah membahas bagaimana membuat project spring-boot dan struktur dari project maka selanjutnya adalah mencoba membuat sebuah rest service sederhana tentang biodata.	
##### a. Set File Properties	

```
server.port = 8080 ❶

# H2
spring.h2.console.enabled=true ❷
spring.h2.console.path=/h2	❸
spring.h2.console.webAllowOthers=true ❹

# Datasource
spring.datasource.url=jdbc:h2:file:~/spring/rest-service ❺
spring.datasource.username=sa ❻
spring.datasource.password= ❼
spring.datasource.driver-class-name=org.h2.Driver ❽
spring.h2.console.enabled=true										
spring.h2.console.settings.web-allow-others=true					

```

❶	Set port yang digunakan untuk project ini <br/>
❷	Set console H2 sebagai database yang digunakan dapat diakses <br/>
❸	Set path console, untuk hal ini maka dapat dibuka setelah project dijalankan dengan path `http://localhost:8080/h2` <br/>
❹	Set untuk console bisa dibuka diluar localhost <br/>
❺	Set datasource url, untuk h2 berupa file <br/>
❻	Set username untuk database <br/>
❼	Set password untuk database <br/>
❽	Set driver class H2 <br/>


##### b. *Create Entity*

Project ini menggunakan JPA dimana untuk akses ke database kita membuat sebuah class entity dimana pada database akan dianggap sebuah tabel. Berdasarkan tema kita akan membuat sebuah service biodata.

Untuk membuat entity maka kita coba buat satu buah folder dalam package com.emerio.rnd.rest dengan nama entity, dan membuat sebuah file dengan nama Biodata.java. berikut adalah struktur setelah pembuatan folder dan file tersebut.

```
|-- .mvn 
|-- .settings
|-- src
	|-- main 
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- rest
							|-- entity 
								|-- Biodata.java
						|-- RestApplication.java
		|-- resources
			|-- static
			|-- templates
			|-- application.properties
	|-- test
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- rest
						|-- RestApplicationTest.java
|-- target
|-- .classpath
|-- .gitignore
|-- .project
|-- mvnw
|-- mvnw.cmd
|-- pom.xml

```


```java
package com.emerio.rnd.rest.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity ❶
public class Biodata 
{
    @Id ❷
    @GeneratedValue ❸
    private Long id;

    private String username;
    private String firstname;
    private String lastname;
    private String address;

    // getter and setter

    public Long getId()
    {
        return this.id;
    }

    public void setId (Long id)
    {
        this.id = id;
    }

    public String getUsername()
    {
        return this.username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getFirstname()
    {
        return this.username;
    }
    public void setFirstname(String firstname)
    {
        this.username = username;
    }

    public String getLastname()
    {
        return this.lastname;
    }

    public void setLastname (String lastname)
    {
        this.lastname = lastname;
    }

    public String getAddress ()
    {
        return this.address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

}

```

Kode diatas adalah kelas *Entity* yang digunakan pada tulisan ini, untuk membuat sebuah kelas *Entity* cukup menambahkan annotation ```@Entity``` sesuai dengan tanda nomor 1 pada kode diatas. Sedangkan untuk memberikan *constraint* terhadap kolom yang dibuat maka kita dapat menggunakan annotasi seperti poin pada nomor 2 dan 3, dimana atribut ```id``` diset sebagai id dan value yang dibuat secara otomatis. untuk lebih detail terkait JPA Annotation dapat dilihat pada link berikut [*link JPA annotation*](https://www.oracle.com/technetwork/middleware/ias/toplink-jpa-annotations-096251.html).

##### c. *CRUD Repository*

*CRUD Repository* adalah *interface* dan *extends Spring data Repository Interface*. Interface ini menyediakan method yang dapat digunakan untuk operasi standard CRUD pada suatu *entity*. *Method* yang telah disediakan beberapa sebagai berikut, 

- `<S extends T> S save(S entity)`: Simpan dan Update suatu entity dan mengembalikan value hasil penyimpanan. <br/>
- `<Optional<T> findById(ID primaryKey)`: Mengembalikan *entity* berdasarkan id *(primary key)*. <br/>
- `Iterable<T> findAll()`: Mengembalikan seluruh *entity* <br/>
- `long count()`: Mengembalikan total data <br/>
- `void delete(T entity)`: Hapus *Entity*. <br/>
- `boolean existsById(ID primaryKey)`: *Method* untuk mengecek apakah *entity* tersedia atau tidak berdasarkan *primary key* yang dimasukan. <br/>.

Untuk membuat *crud-repository* maka kita coba buat satu buah folder dalam package com.emerio.rnd.rest dengan nama repo, dan membuat sebuah file dengan nama BiodataRepo.java. berikut adalah struktur setelah pembuatan folder dan file tersebut.

```
|-- .mvn 
|-- .settings
|-- src
	|-- main 
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- rest
							|-- entity 
								|-- Biodata.java
							|-- repo 
								|-- BiodataRepo.java
						|-- RestApplication.java
		|-- resources
			|-- static
			|-- templates
			|-- application.properties
	|-- test
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- rest
						|-- RestApplicationTest.java
|-- target
|-- .classpath
|-- .gitignore
|-- .project
|-- mvnw
|-- mvnw.cmd
|-- pom.xml

```

```java
package com.emerio.rnd.rest.repo; 

import java.util.Optional;

import com.emerio.rnd.rest.entity.Biodata;

import org.springframework.data.repository.CrudRepository;

public interface BiodataRepo extends CrudRepository<Biodata, Long> ❶
	{
	    Optional<Biodata> findByUsername(String name); ❷
	}
```

Kode diatas adalah implementasi *CrudRepository* pada project ini. kita dapat *custom method* sesuai dengan kebutuhan seperti yang ditunjukan pada poin ❷, dimana pada poin tersebut mengembalikan entity dengan memasukan username. Untuk custom query seperti diatas ada beberapa hal yang harus diperhatikan, contohnya adalah nama item yang diingkan sebagai filter harus sesuai dengan nama atribut yang dideklarasikan pada *entity*. Untuk lebih detail dapat dilihat pada link berikut [crud-repository](https://docs.spring.io/spring-data/jpa/docs/1.4.3.RELEASE/reference/html/jpa.repositories.html).

##### d. *Create Controller*

Bagian yang terakhir dibuat adalah *controller*, dimana pada bagian ini kita akan membuat 4 buah fungsi sesuai tema project yaitu *create, read, update , dan delete*. Untuk membuat controller pada contoh ini dengan cara membuat satu buah folder dalam package com.emerio.rnd.rest dengan nama controller, dan membuat sebuah file dengan nama BiodataController.java. berikut adalah struktur setelah pembuatan folder dan file tersebut.


```
|-- .mvn 
|-- .settings
|-- src
	|-- main 
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- rest
							|-- entity 
								|-- Biodata.java
							|-- repo 
								|-- BiodataRepo.java
							|-- controller 
								|-- BiodataController.java
						|-- RestApplication.java
		|-- resources
			|-- static
			|-- templates
			|-- application.properties
	|-- test
		|-- java
			|-- com 
				|-- emerio
					|-- rnd
						|-- rest
						|-- RestApplicationTest.java
|-- target
|-- .classpath
|-- .gitignore
|-- .project
|-- mvnw
|-- mvnw.cmd
|-- pom.xml

```

Dan berikut adalah kode controller yang digunakan pada project ini.

```java
package com.emerio.rnd.rest.controller;

import java.util.Optional;

import com.emerio.rnd.rest.entity.Biodata;
import com.emerio.rnd.rest.repo.BiodataRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController ❶
public class BiodataController 
{
    private BiodataRepo biodataRepo;

    @Autowired
    BiodataController (BiodataRepo biodataRepo)
    {
        this.biodataRepo = biodataRepo;
    }

	 ❷
    @RequestMapping(method = RequestMethod.GET, value="/biodata", produces = MediaType.APPLICATION_JSON_VALUE) 
    ResponseEntity<Iterable<Biodata>> getAll()
    {
        try
        {   
            return new ResponseEntity<Iterable<Biodata>>(this.biodataRepo.findAll(), HttpStatus.OK);
        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
	 ❸
    @RequestMapping(method = RequestMethod.GET, value="/biodata/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Biodata> getOne(@PathVariable Long id)
    {
        try
        {
            return new ResponseEntity<Biodata>(this.biodataRepo.findById(id).get() , HttpStatus.OK);
        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

	❹
    @RequestMapping(method = RequestMethod.POST , value="/biodata", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Biodata> createOne(@RequestBody Biodata biodata)
    {
        try
        {
            Optional<Biodata> bio = this.biodataRepo.findByUsername(biodata.getUsername());
            if (bio.isPresent())
            {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            } else 
            {
                Biodata bioSave = this.biodataRepo.save(biodata);
                return new ResponseEntity<Biodata>(bioSave, HttpStatus.CREATED);
            }
        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

	 ❺
    @RequestMapping(method = RequestMethod.PUT , value="/biodata/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Biodata> updateOne (@RequestBody Biodata biodata, @PathVariable Long id)
    {
        try
        {
            Optional<Biodata> bio = this.biodataRepo.findByUsername(biodata.getUsername());
            if (bio.isPresent())
            {
                if (biodata.getFirstname() != null)
                {
                    bio.get().setFirstname(biodata.getFirstname());
                }
                if (biodata.getLastname() != null)
                {
                    bio.get().setLastname(biodata.getLastname());
                }
                if (biodata.getAddress() != null)
                {
                    bio.get().setAddress(biodata.getAddress());
                }
                return new ResponseEntity<Biodata>(this.biodataRepo.save(bio.get()), HttpStatus.CREATED);
            } else 
            {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e)
        {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

	 ❻
    @RequestMapping(method = RequestMethod.DELETE, value = "/biodata/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Biodata> deleteOne(@PathVariable Long id )
    {
        try
        {
            Optional<Biodata> bio = this.biodataRepo.findById(id);
            if (bio.isPresent())
            {
                this.biodataRepo.delete(bio.get());
                return new ResponseEntity<Biodata>(bio.get(), HttpStatus.OK);
            } else 
            {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e )
        {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
```

 
##### e. *Run Project*

Untuk menjalankan project, kita dapat menggunakan script `mvn clean install spring-boot:run` pada terminal dimana direktori project disimpan dan terminal akan menunjukan tampilan seperti berikut 

```
................................

2018-10-13 14:50:08.731  INFO 1740 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/webjars/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2018-10-13 14:50:08.732  INFO 1740 --- [           main] o.s.w.s.handler.SimpleUrlHandlerMapping  : Mapped URL path [/**] onto handler of type [class org.springframework.web.servlet.resource.ResourceHttpRequestHandler]
2018-10-13 14:50:08.957  INFO 1740 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Registering beansfor JMX exposure on startup
2018-10-13 14:50:08.959  INFO 1740 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Bean with name 'dataSource' has been autodetected for JMX exposure
2018-10-13 14:50:08.966  INFO 1740 --- [           main] o.s.j.e.a.AnnotationMBeanExporter        : Located MBean 'dataSource': registering with JMX server as MBean [com.zaxxer.hikari:name=dataSource,type=HikariDataSource]
2018-10-13 14:50:09.034  INFO 1740 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started onport(s): 8080 (http) with context path ''
2018-10-13 14:50:09.042  INFO 1740 --- [           main] com.emerio.rnd.rest.RestApplication      : Started RestApplication in 4.887 seconds (JVM running for 18.381)
```

Dan kita dapat test dengan menggunakan postman

1. Buat Biodata 
![](./images/create.png)
2. Mengambil Seluruh Biodata 
![](./images/get-all.png)
3. Mengambil Spesifik Biodata
![](./images/get-one.png)
4. Ubah Biodata
![](./images/update.png)
5. Hapus Biodata
![](./images/delete.png)

**catatan : perhatikan method yang digunakan pada postman.



#### Referensi 

1. [http://spring.io/projects/spring-boot](http://spring.io/projects/spring-boot)
2. personal experience



